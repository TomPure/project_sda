<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
	/**
	 * @return array
	 */
	public function findAll()
	{
		//lista użytkownikow od A-Z
		return $this->findBy([], ['username' => 'ASC']);
	}
}
