<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('@App/default/index.html.twig', ['name' => 'Tom']);
    }

    public function searchPageAction(Request $request)
    {

        return $this->render('@App/search/searchpage.html.twig');
    }

    public function contactPageAction(Request $request)
    {

        return $this->render('@App/contact/contactpage.html.twig');
    }

    public function myaccountPageAction(Request $request)
    {

        return $this->render('@App/myaccount/myaccountpage.html.twig');
    }


}
