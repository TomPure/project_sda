<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserDataForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user_name',TextType::class, ['attr' => [ 'maxlength' => 25 ] ] )
            ->add('user_surname',TextType::class, ['attr' => [ 'maxlength' => 50 ] ] )
            ->add('user_cellular',TextType::class, ['attr' => [ 'maxlength' => 20 ] ] );
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_user_data_form';
    }
}
